#include "mainwindow/mainwindow.h"
#include "startwindow.h"

#include <QApplication>

int
main(int argc, char* argv[])
{
  git_libgit2_init();
  QApplication a(argc, argv);
  StartWindow* s = new StartWindow;
  MainWindow* w = new MainWindow;
  QObject::connect(s,
                   SIGNAL(sendData(QString, QString, QString)),
                   w,
                   SLOT(receiveData(QString, QString, QString)));
  s->show();
  git_libgit2_shutdown();
  return a.exec();
}
