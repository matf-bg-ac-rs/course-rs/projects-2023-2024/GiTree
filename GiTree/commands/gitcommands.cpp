#include "gitcommands.h"
#include <cstring>
#include <iostream>
static void
check_error(int error_code, const char* action)
{
  if (error_code) {
    std::cerr << "Error " << error_code << " " << action << std::endl;
  }
}

GitCommands::GitCommands(const std::string path,
                         const std::string userName,
                         const std::string userEmail)
  : m_path(path)
  , m_userName(userName)
  , m_userEmail(userEmail)
{
  git_libgit2_init();
}

GitCommands::~GitCommands()
{
  closeRepository();
  git_libgit2_shutdown();
}

void
GitCommands::initializeRepository()
{
  check_error(git_repository_init(&m_repo, m_path.c_str(), false),
              "Repository init");
}

void
GitCommands::addPathToIndex(const std::string& path, bool isFile)
{

  git_index* index = nullptr;

  check_error(git_repository_index(&index, m_repo), "Get index");
  if (isFile) {
    check_error(git_index_add_bypath(index, path.c_str()), "Add file to index");
  } else {
    git_strarray pathspec;
    pathspec.count = 1;
    pathspec.strings = (char**)&path;
    check_error(git_index_add_all(
                  index, &pathspec, GIT_INDEX_ADD_DEFAULT, nullptr, nullptr),
                "Add content of the repo in stage");
  }

  check_error(git_index_write(index), "Write index");

  git_index_free(index);
}

errorCodes::Indicator
GitCommands::executeCommit(const std::string& commitMessage)
{
  git_index* index;
  git_oid tree_id, commit_id, parent_id;
  git_signature *author, *committer;
  git_tree* tree;
  git_commit* parent_commit = nullptr;
  errorCodes::Indicator ind = errorCodes::SUCCESS;

  check_error(git_repository_index(&index, m_repo), "Get index");

  check_error(git_index_write_tree(&tree_id, index), "Writing tree");
  check_error(git_index_write(index), "Git index write");
  check_error(git_tree_lookup(&tree, m_repo, &tree_id), "Tree lookup");

  if (git_reference_name_to_id(&parent_id, m_repo, "HEAD") == 0) {
    check_error(git_commit_lookup(&parent_commit, m_repo, &parent_id),
                "Commit lookup");
  }

  check_error(
    git_signature_now(&author, m_userName.c_str(), m_userEmail.c_str()),
    "Git signature for author");
  check_error(
    git_signature_now(&committer, m_userName.c_str(), m_userEmail.c_str()),
    "Git signature for committer");

  git_status_options options = GIT_STATUS_OPTIONS_INIT;
  options.show = GIT_STATUS_SHOW_INDEX_ONLY;
  git_status_list* status = nullptr;
  check_error(git_status_list_new(&status, m_repo, &options), "Status list");
  size_t count = git_status_list_entrycount(status);

  if (count > 0) {
    check_error(git_commit_create_v(&commit_id,
                                    m_repo,
                                    "HEAD",
                                    author,
                                    committer,
                                    NULL,
                                    commitMessage.c_str(),
                                    tree,
                                    parent_commit ? 1 : 0,
                                    parent_commit ? parent_commit : nullptr),
                "Create commit");
  } else {
    ind = errorCodes::EINDEXEMPTY;
  }

  git_status_list_free(status);
  git_index_free(index);
  git_commit_free(parent_commit);
  git_tree_free(tree);
  git_index_free(index);
  git_signature_free(author);
  git_signature_free(committer);
  return ind;
}

void
GitCommands::configureUser()
{
  git_config* cfg = nullptr;
  check_error(git_config_open_default(&cfg), "Opening default configuration");

  check_error(git_config_set_string(cfg, "user.name", m_userName.c_str()),
              "Name configuration");
  check_error(git_config_set_string(cfg, "user.email", m_userEmail.c_str()),
              "Email configuration");

  git_config_free(cfg);
}

errorCodes::Indicator
GitCommands::createBranch(const std::string& branchName)
{
  git_reference* ref = nullptr;
  git_oid commit_id;
  git_commit* curr_commit = nullptr;
  errorCodes::Indicator ind = errorCodes::SUCCESS;

  check_error(git_reference_name_to_id(&commit_id, m_repo, "HEAD"),
              "Reference of the commit");
  check_error(git_commit_lookup(&curr_commit, m_repo, &commit_id),
              "Commit lookup");
  if (git_branch_lookup(&ref, m_repo, branchName.c_str(), GIT_BRANCH_LOCAL) ==
      0)
    ind = errorCodes::EEXISTS;
  else if (git_branch_create(
             &ref, m_repo, branchName.c_str(), curr_commit, false) ==
           GIT_EINVALIDSPEC)
    ind = errorCodes::EINVALIDSPEC;

  git_commit_free(curr_commit);
  git_reference_free(ref);
  return ind;
}

errorCodes::Indicator
GitCommands::checkoutToBranch(const std::string& branchName)
{
  git_commit* targetCommit = nullptr;
  git_reference* branchRef;
  errorCodes::Indicator ind = errorCodes::SUCCESS;
  const git_oid* targetId;
  git_checkout_options checkout_opts;

  if (git_branch_lookup(
        &branchRef, m_repo, branchName.c_str(), GIT_BRANCH_LOCAL) ==
      GIT_ENOTFOUND) {
    ind = errorCodes::ENOTFOUND;
    git_reference_free(branchRef);
    git_repository_free(m_repo);
    return ind;
  }
  targetId = git_reference_target(branchRef);
  check_error(targetId == nullptr, "Reference target");

  checkout_opts = GIT_CHECKOUT_OPTIONS_INIT;
  checkout_opts.checkout_strategy = GIT_CHECKOUT_FORCE;
  check_error(git_commit_lookup(&targetCommit, m_repo, targetId),
              "Commit lookup");
  check_error(
    git_checkout_tree(m_repo, (const git_object*)targetCommit, &checkout_opts),
    "Checkout tree");
  check_error(
    git_repository_set_head(m_repo, ("refs/heads/" + branchName).c_str()),
    "Set head");

  git_commit_free(targetCommit);
  git_reference_free(branchRef);
  return ind;
}

errorCodes::Indicator
GitCommands::createBranchAndCheckout(const std::string& branchName)
{
  errorCodes::Indicator ind = errorCodes::SUCCESS;
  ind = createBranch(branchName);
  if (ind != errorCodes::SUCCESS)
    return ind;
  return checkoutToBranch(branchName);
}

errorCodes::Indicator
GitCommands::executeMerge(const std::string& branchName,
                          std::vector<std::string>& conflictsList)
{
  git_reference *branchRef, *headRef;
  git_annotated_commit* targetCommit;
  git_index* index;
  git_merge_analysis_t analysis;
  git_merge_preference_t preference;
  errorCodes::Indicator ind = errorCodes::SUCCESS;

  if (git_branch_lookup(
        &branchRef, m_repo, branchName.c_str(), GIT_BRANCH_LOCAL) ==
      GIT_ENOTFOUND) {
    ind = errorCodes::ENOTFOUND;
    git_reference_free(branchRef);
    return ind;
  }
  const git_oid* targetId = git_reference_target(branchRef);
  check_error(targetId == nullptr, "Reference target");
  check_error(git_repository_head(&headRef, m_repo), "Get HEAD reference");
  check_error(git_annotated_commit_lookup(&targetCommit, m_repo, targetId),
              "Commit lookup");

  check_error(git_merge_analysis(&analysis,
                                 &preference,
                                 m_repo,
                                 (const git_annotated_commit**)(&targetCommit),
                                 1),
              "Analysis");

  if (analysis & GIT_MERGE_ANALYSIS_UP_TO_DATE) {
    git_reference_free(headRef);
    git_reference_free(branchRef);
    return errorCodes::EUPTODATE;
  } else if ((analysis & GIT_MERGE_ANALYSIS_FASTFORWARD &&
              !(preference & GIT_MERGE_PREFERENCE_NO_FASTFORWARD))) {

    performFastforward(targetId);
    git_reference_free(headRef);
    git_reference_free(branchRef);
    return errorCodes::SUCCESS;
  } else if (analysis & GIT_MERGE_ANALYSIS_NORMAL) {
    git_merge_options mergeOpts = GIT_MERGE_OPTIONS_INIT;
    git_checkout_options checkoutOpts = GIT_CHECKOUT_OPTIONS_INIT;

    mergeOpts.file_flags = GIT_MERGE_FILE_STYLE_DIFF3;
    checkoutOpts.checkout_strategy =
      GIT_CHECKOUT_FORCE | GIT_CHECKOUT_ALLOW_CONFLICTS;

    check_error(git_merge(m_repo,
                          (const git_annotated_commit**)(&targetCommit),
                          1,
                          &mergeOpts,
                          &checkoutOpts),
                "Merge");
  }
  check_error(git_repository_index(&index, m_repo), "Repository index");

  if (git_index_has_conflicts(index)) {
    conflictsList = getConflictingFiles();
    ind = errorCodes::ECONFLICTS;
  } else {
    const char* targetBranchName;
    git_branch_name(&targetBranchName, headRef);
    std::string commitMessage =
      "Merge branch '" + branchName + "' into '" + targetBranchName + "'";
    executeMergeCommit(commitMessage, branchName);
  }
  git_reference_free(headRef);
  git_reference_free(branchRef);
  git_index_free(index);
  git_annotated_commit_free(targetCommit);

  return ind;
}

void
GitCommands::openRepository()
{
  check_error(git_repository_open(&m_repo, m_path.c_str()), "Open repository");
}

void
GitCommands::closeRepository()
{
  git_repository_free(m_repo);
}
void
GitCommands::executeMergeCommit(const std::string& commitMessage,
                                const std::string& branchName)
{
  git_index* index;
  git_oid tree_id, commit_id, currId;
  git_signature *author, *committer;
  git_tree* tree;
  git_reference* headRef;
  git_reference* branchRef = nullptr;
  git_commit* parents[2] = { nullptr, nullptr };

  check_error(git_repository_index(&index, m_repo), "Get index");

  check_error(git_repository_head(&headRef, m_repo), "Get HEAD reference");
  check_error(
    git_reference_peel((git_object**)&parents[0], headRef, GIT_OBJECT_COMMIT),
    "Peel");

  check_error(
    git_branch_lookup(&branchRef, m_repo, branchName.c_str(), GIT_BRANCH_LOCAL),
    "Branch lookup");
  const git_oid* targetId = git_reference_target(branchRef);
  check_error(git_commit_lookup(&parents[1], m_repo, targetId), "Lookup");

  check_error(
    git_signature_now(&author, m_userName.c_str(), m_userEmail.c_str()),
    "Git signature for author");
  check_error(
    git_signature_now(&committer, m_userName.c_str(), m_userEmail.c_str()),
    "Git signature for committer");

  check_error(git_index_write_tree(&tree_id, index), "Writing tree");
  check_error(git_tree_lookup(&tree, m_repo, &tree_id), "Tree lookup");

  check_error(git_commit_create(&commit_id,
                                m_repo,
                                "HEAD",
                                author,
                                committer,
                                NULL,
                                commitMessage.c_str(),
                                tree,
                                2,
                                (const git_commit**)parents),
              "Create commit");

  git_tree_free(tree);
  git_repository_state_cleanup(m_repo);
  git_signature_free(author);
  git_signature_free(committer);
  git_commit_free(parents[0]);
  git_commit_free(parents[1]);
}
void
GitCommands::performFastforward(const git_oid* targetId)
{
  git_checkout_options checkoutOpts = GIT_CHECKOUT_OPTIONS_INIT;
  git_reference* headRef;
  git_reference* newHeadRef;
  git_object* target = nullptr;

  check_error(git_repository_head(&headRef, m_repo), "Get HEAD reference");
  check_error(git_object_lookup(&target, m_repo, targetId, GIT_OBJECT_COMMIT),
              "Object lookup");

  checkoutOpts.checkout_strategy = GIT_CHECKOUT_SAFE;
  check_error(git_checkout_tree(m_repo, target, &checkoutOpts),
              "Checkout tree");
  check_error(git_reference_set_target(
                &newHeadRef, headRef, targetId, "merge: Fast-Forward"),
              "Set target");

  git_reference_free(headRef);
  git_reference_free(newHeadRef);
  git_object_free(target);
}

std::vector<std::string>
GitCommands::getConflictingFiles()
{

  std::vector<std::string> conflictsList;
  git_index_conflict_iterator* conflicts;
  const git_index_entry *anchestor, *ours, *theirs;
  git_index* index;
  check_error(git_repository_index(&index, m_repo), "Repository index");
  git_index_conflict_iterator_new(&conflicts, index);
  while (git_index_conflict_next(&anchestor, &ours, &theirs, conflicts) == 0) {
    if (ours)
      conflictsList.push_back(ours->path);
  }
  git_index_conflict_iterator_free(conflicts);
  git_index_free(index);
  return conflictsList;
}
std::string
GitCommands::getPath()
{
  return m_path;
}
std::string
GitCommands::getUserName()
{
  return m_userName;
}
std::string
GitCommands::getUserEmail()
{
  return m_userEmail;
}

bool
GitCommands::isFileInStagingArea(const char* filePath)
{
  git_status_options options = GIT_STATUS_OPTIONS_INIT;
  options.show = GIT_STATUS_SHOW_INDEX_ONLY;
  git_status_list* status = nullptr;
  check_error(git_status_list_new(&status, m_repo, &options), "Status list");
  size_t count = git_status_list_entrycount(status);

  for (size_t i = 0; i < count; ++i) {
    const git_status_entry* entry = git_status_byindex(status, i);
    if (entry && strcmp(entry->head_to_index->new_file.path, filePath) == 0) {
      git_status_list_free(status);
      return true;
    }
  }
  git_status_list_free(status);
  return false;
}
