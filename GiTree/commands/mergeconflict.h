#ifndef MERGECONFLICTWIDGET_H
#define MERGECONFLICTWIDGET_H

#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QKeyEvent>
#include <QMimeDatabase>
#include <QMimeType>
#include <QTextEdit>
#include <QTextStream>

class MergeConflictWidget : public QTextEdit
{
  Q_OBJECT
public:
  MergeConflictWidget(QWidget* parent = nullptr);
  void setPath(QString& path);
signals:
  void closeTab(QString filePath);

protected:
  void keyPressEvent(QKeyEvent* event) override;

private:
  QString m_filePath;
  QString readFile(QString& path);
};

#endif // MERGECONFLICTWIDGET_H
