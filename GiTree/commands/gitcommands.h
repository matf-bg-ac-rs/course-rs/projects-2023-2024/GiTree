#ifndef GITCOMMANDS_H
#define GITCOMMANDS_H

#include <git2.h>
#include <string>
#include <vector>

namespace errorCodes {
enum Indicator
{
  SUCCESS,
  EINVALIDSPEC,
  ENOTFOUND,
  EINDEXEMPTY,
  EEXISTS,
  EUPTODATE,
  ECONFLICTS
};
};

class GitCommands
{
public:
  GitCommands();
  ~GitCommands();
  GitCommands(const std::string path,
              const std::string userName,
              const std::string userEmail);
  void initializeRepository();
  void addPathToIndex(const std::string& path, bool isFile);
  void configureUser();
  errorCodes::Indicator executeCommit(const std::string& commitMessage);
  errorCodes::Indicator createBranch(const std::string& branchName);
  errorCodes::Indicator checkoutToBranch(const std::string& branchName);
  errorCodes::Indicator createBranchAndCheckout(const std::string& branchName);
  errorCodes::Indicator executeMerge(const std::string& branchName,
                                     std::vector<std::string>& conflictsList);
  void openRepository();
  void closeRepository();
  void executeMergeCommit(const std::string& commitMessage,
                          const std::string& branchName);
  std::vector<std::string> getConflictingFiles();
  std::string getPath();
  std::string getUserName();
  std::string getUserEmail();

private:
  std::string m_path;
  std::string m_userName;
  std::string m_userEmail;
  git_repository* m_repo;
  void performFastforward(const git_oid* targetId);
  bool isFileInStagingArea(const char* filePath);
};

#endif // GITCOMMANDS_H
