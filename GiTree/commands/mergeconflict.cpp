#include "mergeconflict.h"

MergeConflictWidget::MergeConflictWidget(QWidget* parent)
  : QTextEdit(parent)
{
  m_filePath = QString("");
  this->setText(this->readFile(m_filePath));
  connect(this,
          SIGNAL(closeTab(QString)),
          this->parent()->parent()->parent()->parent(),
          SLOT(closeTab(QString)));
}

void
MergeConflictWidget::setPath(QString& path)
{
  this->setText(this->readFile(path));
  return;
}

void
MergeConflictWidget::keyPressEvent(QKeyEvent* event)
{
  if (event->modifiers() == Qt::ControlModifier && event->key() == Qt::Key_S) {
    QString text = this->toPlainText();
    QFile file(this->m_filePath);
    if (file.open(QIODevice::WriteOnly | QIODevice::Text)) {
      QTextStream out(&file);
      out << text;
      file.close();
    }
    emit closeTab(this->m_filePath);
  } else {
    QTextEdit::keyPressEvent(event);
  }
}

QString
MergeConflictWidget::readFile(QString& filePath)
{
  if (filePath.isEmpty())
    return {};
  this->m_filePath = filePath;
  QFile file(filePath);

  if (file.size() >= 4'000'000)
    return QString("File size is too big.");

  if (!file.open(QIODevice::ReadWrite | QIODevice::Text))
    return QString("Error opening the File!");

  QTextStream in(&file);
  QString allText = in.readAll();
  if (allText.isEmpty())
    return QString("File is empty!");
  return allText;
}
