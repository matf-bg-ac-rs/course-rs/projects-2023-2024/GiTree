#include "browser.h"
#include "ui_browser.h"

Browser::Browser(QWidget* parent)
  : QMainWindow(parent)
  , ui(new Ui::Browser)
{
  ui->setupUi(this);

  QString path = "/home";
  m_model = new QFileSystemModel(this);
  m_model->setRootPath(path);
  ui->treeView->setModel(m_model);
  ui->treeView->setRootIndex(m_model->index(path));
  ui->treeView->header()->setSectionResizeMode(QHeaderView::ResizeToContents);
  ui->treeView->setSelectionMode(QTreeView::SingleSelection);
  ui->treeView->setColumnHidden(2, true);
}
Browser::~Browser()
{
  delete ui;
}

void
Browser::on_pbFinish_clicked()
{
  QModelIndexList indexes = ui->treeView->selectionModel()->selectedIndexes();

  m_currentPath = m_model->fileInfo(indexes[0]).absoluteFilePath();

  emit sendPath(m_currentPath);

  this->close();
}
