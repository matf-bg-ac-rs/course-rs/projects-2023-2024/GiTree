#ifndef BROWSER_H
#define BROWSER_H

#include <QFileSystemModel>
#include <QMainWindow>

namespace Ui {
class Browser;
}

class Browser : public QMainWindow
{
  Q_OBJECT

public:
  explicit Browser(QWidget* parent = nullptr);
  ~Browser();

signals:
  void sendPath(QString& currentPath);

private slots:

  void on_pbFinish_clicked();

private:
  Ui::Browser* ui;
  QFileSystemModel* m_model;
  QString m_currentPath;
};

#endif // BROWSER_H
