#ifndef SHOWREPO_H
#define SHOWREPO_H

#include <QDir>
#include <QFileIconProvider>
#include <QFileInfo>
#include <QFileSystemModel>
#include <QHeaderView>
#include <QString>
#include <QStyledItemDelegate>
#include <QTreeView>
#include <QTreeWidget>
#include <QWidget>
#include <git2.h>

namespace colorCodes {
enum ColorIndicator
{
  IIWDIR,
  IININDEX,
  IUPTODATE
};
};

class ModifiedTreeView : public QTreeView
{
  Q_OBJECT
public:
  ModifiedTreeView(QWidget* parent = nullptr);
};

class CustomFileSystemModel : public QFileSystemModel
{
  Q_OBJECT
public:
  CustomFileSystemModel(QObject* parent = nullptr);
  QVariant data(const QModelIndex& index, int role) const override;
  void setPaths(const std::vector<QString>& newPaths);

private:
  colorCodes::ColorIndicator getFileStatus(QString filePath,
                                           QString dirPath) const;
  std::vector<QString> m_paths;
};

class ShowRepoWidget : public QTreeWidget
{
  Q_OBJECT

public:
  ShowRepoWidget(QWidget* parent = nullptr);
  ~ShowRepoWidget();
  bool setViewPath(QString& path);
  QString getSelectedItemPath(bool relative = true);
  std::vector<QString> getAllPaths();
public slots:
  void filesInserted();

private:
  ModifiedTreeView* m_treeView;
  CustomFileSystemModel* m_model;
};

#endif // SHOWREPO_H
