#include "showrepo.h"
ModifiedTreeView::ModifiedTreeView(QWidget* parent)
  : QTreeView(parent)
{
  setHeaderHidden(true);
  setSelectionMode(QTreeView::SingleSelection);
  setAnimated(true);
  setIndentation(20);
  setSortingEnabled(true);
  header()->setSectionResizeMode(QHeaderView::ResizeToContents);
  setExpandsOnDoubleClick(false);
}

CustomFileSystemModel::CustomFileSystemModel(QObject* parent)
  : QFileSystemModel(parent)
{
  setFilter(QDir::AllEntries | QDir::NoDotAndDotDot);
  QFileIconProvider* provider = new QFileIconProvider();
  setIconProvider(provider);
}
QVariant
CustomFileSystemModel::data(const QModelIndex& index, int role) const
{
  if (index.isValid() && role == Qt::ForegroundRole) {
    QString filePath = this->filePath(index);
    if (filePath.contains(this->rootPath())) {
      QString fileName = QDir(this->rootPath()).relativeFilePath(filePath);

      if (std::find(m_paths.begin(), m_paths.end(), filePath) !=
          m_paths.end()) {
        colorCodes::ColorIndicator ind =
          getFileStatus(fileName, this->rootPath());
        if (ind == colorCodes::IININDEX)
          return QVariant(QColor(Qt::green));
        else if (ind == colorCodes::IIWDIR)
          return QVariant(QColor(Qt::red));

        return QVariant(QColor(Qt::blue));
      }
    }
  }

  return QFileSystemModel::data(index, role);
}
void
CustomFileSystemModel::setPaths(const std::vector<QString>& newPaths)
{
  m_paths = newPaths;
}
colorCodes::ColorIndicator
CustomFileSystemModel::getFileStatus(QString filePath, QString dirPath) const
{
  git_libgit2_init();

  git_repository* repo;
  colorCodes::ColorIndicator ind = colorCodes::IUPTODATE;
  unsigned status;
  git_repository_open(&repo, dirPath.toStdString().c_str());
  git_status_file(&status, repo, filePath.toStdString().c_str());
  if ((status & GIT_STATUS_WT_MODIFIED) || (status & GIT_STATUS_WT_NEW) ||
      (status & GIT_STATUS_WT_DELETED) || (status & GIT_STATUS_WT_RENAMED))
    ind = colorCodes::IIWDIR;
  else if ((status & GIT_STATUS_INDEX_MODIFIED) ||
           (status & GIT_STATUS_INDEX_NEW) ||
           (status & GIT_STATUS_INDEX_DELETED) ||
           (status & GIT_STATUS_WT_RENAMED))
    ind = colorCodes::IININDEX;
  else if (status & GIT_STATUS_CURRENT)
    ind = colorCodes::IUPTODATE;

  git_repository_free(repo);
  git_libgit2_shutdown();
  return ind;
}

ShowRepoWidget::ShowRepoWidget(QWidget* parent)
  : QTreeWidget(parent)
{
  m_treeView = new ModifiedTreeView(this);
  m_model = new CustomFileSystemModel(this);
  QString path = QDir::homePath();
  QModelIndex index = m_model->index(path);
  m_model->setRootPath(path);

  m_treeView->setModel(m_model);
  m_treeView->setRootIndex(index);
  m_treeView->setGeometry(0, 0, 299, 702);
  m_treeView->setColumnHidden(1, true);
  m_treeView->setColumnHidden(2, true);
  m_treeView->setColumnHidden(3, true);
  QObject::connect(m_model,
                   &CustomFileSystemModel::layoutChanged,
                   this,
                   &ShowRepoWidget::filesInserted);
}

ShowRepoWidget::~ShowRepoWidget()
{
  delete m_treeView;
  delete m_model;
}

bool
ShowRepoWidget::setViewPath(QString& path)
{
  this->m_model->setRootPath(path);
  this->m_treeView->setRootIndex(this->m_model->index(path));
  std::vector<QString> paths = getAllPaths();
  this->m_model->setPaths(paths);
  return true; // Implement check for rootPathChanged() signal and QModelIndex
               // check
}

QString
ShowRepoWidget::getSelectedItemPath(bool relative)
{
  QModelIndexList indexes =
    this->m_treeView->selectionModel()->selectedIndexes();
  if (indexes.length() == 0) {
    return QString("");
  }
  QFileInfo info = this->m_model->fileInfo(indexes[0]);
  if (relative)
    return QDir(this->m_model->rootPath())
      .relativeFilePath(info.absoluteFilePath());
  else
    return info.absoluteFilePath();
}
std::vector<QString>
ShowRepoWidget::getAllPaths()
{
  std::vector<QString> paths;
  QModelIndex rootIndex = m_treeView->rootIndex();

  if (rootIndex.isValid()) {

    QString rootPath = m_model->filePath(rootIndex);
    paths.push_back(rootPath);

    QDirIterator it(rootPath,
                    QDir::NoDotAndDotDot | QDir::AllEntries | QDir::Hidden,
                    QDirIterator::Subdirectories);
    while (it.hasNext()) {
      it.next();
      if (it.fileInfo().isFile()) {
        paths.push_back(it.filePath());
      }
    }
  }

  return paths;
}

void
ShowRepoWidget::filesInserted()
{
  std::vector<QString> newPaths = getAllPaths();
  m_model->setPaths(newPaths);
}
