#include "../graph/edge.hpp"
#include <QPainter>

#include "utility.hpp"

Edge::Edge(const QString& label, const QFont& label_font)
  : m_label(label)
  , m_labelFont(label_font)
{
}

QRectF
Edge::boundingRect() const
{
  return m_boundingRectangle;
};

QString
Edge::getLabel() const
{
  return m_label;
}

QFont
Edge::getLabelFont() const
{
  return m_labelFont;
}

void
Edge::paint(QPainter* painter,
            const QStyleOptionGraphicsItem* option,
            QWidget* widget)
{
  painter->drawPath(m_path);
  painter->setFont(m_labelFont);
  painter->drawText(m_labelPosition, m_label);
}

void
Edge::setup()
{
  set_gv_attribute(m_gvEdge, "label", m_label);
  set_gv_attribute(m_gvEdge, "fontname", m_labelFont.family());
  set_gv_attribute(
    m_gvEdge, "fontsize", QString::number(m_labelFont.pointSize()));
}

void
Edge::update_positions()
{
  m_path.clear();

  bezier* curve_data = ED_spl(m_gvEdge)->list;

  m_path.moveTo(gv_to_qt_coords(curve_data->list[0]));
  for (auto i = 1; i < curve_data->size; i += 3)
    m_path.cubicTo(gv_to_qt_coords(curve_data->list[i]),
                   gv_to_qt_coords(curve_data->list[i + 1]),
                   gv_to_qt_coords(curve_data->list[i + 2]));

  if (curve_data->eflag) {
    QPointF arrow_start =
      gv_to_qt_coords(curve_data->list[curve_data->size - 1]);
    QPointF arrow_end = gv_to_qt_coords(curve_data->ep);
    m_path.lineTo(arrow_end);

    QLineF normal = QLineF(arrow_start, arrow_end).normalVector();
    QPointF arrowhead_vector = QPointF(normal.dx() / 2.0, normal.dy() / 2.0);

    QPolygonF arrowhead({ arrow_start - arrowhead_vector,
                          arrow_end,
                          arrow_start + arrowhead_vector });
    m_path.addPolygon(arrowhead);
  }

  if (m_label != "") {
    QPointF center_position = gv_to_qt_coords(ED_label(m_gvEdge)->pos);
    QPointF label_dim = gv_to_qt_coords(ED_label(m_gvEdge)->dimen);
    m_labelPosition = { center_position.x() - label_dim.x() / 2.0,
                        center_position.y() };
  }

  QRectF label_br = QFontMetrics(m_labelFont)
                      .boundingRect(m_label)
                      .translated(m_labelPosition.x(), m_labelPosition.y());
  m_boundingRectangle = m_path.boundingRect() | label_br;
}
