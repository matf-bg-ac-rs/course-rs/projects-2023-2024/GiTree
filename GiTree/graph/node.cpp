#include "node.hpp"

#include "utility.hpp"
#include <QPainter>

Node::Node(qreal width,
           qreal height,
           const QString& label,
           const QFont& label_font)
  : m_width(width)
  , m_height(height)
  , m_label(label)
  , m_labelFont(label_font)
{
}

QRectF
Node::boundingRect() const
{
  return m_boundingRectangle;
}

qreal
Node::getWidth() const
{
  return m_width;
}

qreal
Node::getHeight() const
{
  return m_height;
}

QString
Node::getLabel() const
{
  return m_label;
}

QFont
Node::getLabelFont() const
{
  return m_labelFont;
}

void
Node::paint(QPainter* painter,
            const QStyleOptionGraphicsItem* option,
            QWidget* widget)
{
  painter->drawPath(m_path);
  painter->setFont(m_labelFont);
  painter->drawText(m_labelPosition, m_label);
}

void
Node::setup()
{
  set_gv_attribute(m_gvNode, "fixedsize", "true");
  set_gv_attribute(m_gvNode, "width", QString::number(m_width / 92.0));
  set_gv_attribute(m_gvNode, "height", QString::number(m_height / 92.0));
  set_gv_attribute(m_gvNode, "label", m_label);
  set_gv_attribute(m_gvNode, "fontname", m_labelFont.family());
  set_gv_attribute(
    m_gvNode, "fontsize", QString::number(m_labelFont.pointSize()));
}

void
Node::update_positions()
{
  m_path.clear();

  qreal width = gv_to_qt_size(ND_width(m_gvNode));
  qreal height = gv_to_qt_size(ND_height(m_gvNode));
  QPointF center = gv_to_qt_coords(ND_coord(m_gvNode));

  QPointF top_left = { center.x() - width / 2.0, center.y() - height / 2.0 };
  QPointF bottom_right = { top_left.x() + width, top_left.y() + height };
  m_path.addEllipse(QRectF(top_left, bottom_right));

  if (m_label != "")
    m_labelPosition = m_path.boundingRect().center() -
                      gv_to_qt_coords(ND_label(m_gvNode)->dimen) / 2.0;

  QRectF label_br = QFontMetrics(m_labelFont)
                      .boundingRect(m_label)
                      .translated(m_labelPosition.x(), m_labelPosition.y());
  m_boundingRectangle = m_path.boundingRect() | label_br;
}
