#include "graphview.h"
#include <sstream>

GraphViewWidget::GraphViewWidget(QWidget* parent)
  : QGraphicsView(parent)
{
  this->m_scene = new CustomScene();
  this->m_graph = new Graph();
  this->m_scene->addItem(this->m_graph);
  this->setScene(this->m_scene);
}

GraphViewWidget::~GraphViewWidget()
{
  m_scene->removeItem(m_graph);
  delete m_scene;
  delete m_graph;
}
void
GraphViewWidget::updatedGraph(
  std::map<std::pair<int, git_oid>, std::set<std::pair<int, git_oid>>> parents,
  git_oid headId,
  std::map<std::string, git_oid> branchRefs)
{
  setGraph(parents, headId, branchRefs);
}

void
GraphViewWidget::setGraph(
  std::map<std::pair<int, git_oid>, std::set<std::pair<int, git_oid>>> parents,
  git_oid headId,
  std::map<std::string, git_oid> branchRefs)
{
  delete m_graph;
  Graph* graph = new Graph();
  std::map<std::string, Node*> idExists;

  int i = 0;
  for (const auto& parent : parents) {
    Node* parentNode;
    std::string parentSHA = git_oid_tostr_s(&parent.first.second);
    // ako nisi dodao cvor
    if (!idExists[parentSHA]) {
      // da li je head
      if (git_oid_equal(&headId, &parent.first.second))
        parentNode =
          new Node(40, 40, QString("C") + QString::number(i++) + QString("*"));
      else
        parentNode = new Node(40, 40, QString("C") + QString::number(i++));
      idExists[parentSHA] = parentNode;
      graph->addNode(parentNode);
    } else
      parentNode = idExists[parentSHA];

    for (const auto& child : parent.second) {
      Node* childNode;
      std::string childSHA = git_oid_tostr_s(&child.second);
      if (!idExists[childSHA]) {

        if (git_oid_equal(&headId, &child.second))
          childNode = new Node(
            40, 40, QString("C") + QString::number(i++) + QString("*"));
        else
          childNode = new Node(40, 40, QString("C") + QString::number(i++));
        idExists[childSHA] = childNode;
        graph->addNode(childNode);
      } else
        childNode = idExists[childSHA];
      // label
      std::stringstream ss;
      for (const auto& branch : branchRefs) {
        if (git_oid_equal(&branch.second, &child.second)) {
          ss << "[" << branch.first << "]" << std::endl;
        }
      }
      Edge* edge = new Edge(QString::fromStdString(ss.str()));
      graph->addEdge(edge, parentNode, childNode);
    }
  }

  this->m_scene->setSceneRect(0, 0, 1000, 86 * i);
  graph->composeLayout();
  this->m_graph = graph;
  this->m_scene->addItem(m_graph);
  this->setScene(m_scene);
  return;
}

QList<QGraphicsItem*>
GraphViewWidget::getSelectedItems()
{
  QList<QGraphicsItem*> list = this->m_scene->selectedItems();
  this->m_scene->clearSelection();
  return list;
}

void
GraphViewWidget::graphChanged(Graph* graph)
{
  this->m_scene->removeItem(this->m_graph);
  // this->setGraph();
  return;
}

CustomScene::CustomScene(QObject* parent)
  : QGraphicsScene(parent)
{
  QRectF sceneRect = QRectF(0, 0, 600, 400);
  setSceneRect(sceneRect);
  qreal padding = 70.0;
  sceneRect.adjust(-padding, -padding, padding, padding);
  setSceneRect(sceneRect);
  setStickyFocus(true);
  // setBackgroundBrush(Qt::white);
}
