#ifndef GRAPH_HPP
#define GRAPH_HPP

#include <QGraphicsItem>
#include <graphviz/gvc.h>

#include "node.hpp"
#include "edge.hpp"

class Graph : public QGraphicsItem
{
public:
    Graph();
    ~Graph();

    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

    bool addNode(Node *node);
    bool addEdge(Edge *edge, Node *src, Node *dst);
    void composeLayout();

private:
    class Context
    {
    public:
        Context();
        ~Context();

        GVC_t *m_gvContext;
    };

    QRectF m_boundingRectangle;

    static Context m_context;
    QVector<Node *> m_nodes;
    QVector<Edge *> m_edges;
    Agraph_t *m_gvGraph;

};

#endif // GRAPH_HPP
