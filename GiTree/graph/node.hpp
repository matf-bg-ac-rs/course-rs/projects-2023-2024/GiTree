#ifndef NODE_HPP
#define NODE_HPP

#include <QFont>
#include <QGraphicsItem>
#include <graphviz/gvc.h>


class Node : public QGraphicsItem
{
    friend class Graph;
public:
    Node(qreal width, qreal height, const QString &label, const QFont &label_font = QFont("Times-Roman", 8));

    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
    qreal getWidth() const;
    qreal getHeight() const;
    QString getLabel() const;
    QFont getLabelFont() const;



private:
    void setup();
    void update_positions();

    qreal m_width;
    qreal m_height;
    QString m_label;
    QFont m_labelFont;

    QPainterPath m_path;
    QPointF m_labelPosition;
    QRectF m_boundingRectangle;

    Agnode_t *m_gvNode;

};
#endif // NODE_HPP
