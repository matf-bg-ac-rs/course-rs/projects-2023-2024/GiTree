#ifndef UTILITY_HPP
#define UTILITY_HPP

#include <QPointF>
#include <QString>
#include <graphviz/gvc.h>

QPoint gv_to_qt_coords(const pointf &gv_point);
qreal gv_to_qt_size(double gv_size);
void set_gv_attribute(void *gv_component, const QString &attribute, const QString &value);


#endif // UTILITY_HPP
