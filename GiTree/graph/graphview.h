#ifndef GRAPHVIEWWIDGET_H
#define GRAPHVIEWWIDGET_H
#include "graph.hpp"
#include <QGraphicsView>
#include <QList>
#include <git2.h>
#include <map>
#include <set>

class CustomScene : public QGraphicsScene
{
  Q_OBJECT
public:
  CustomScene(QObject* parent = nullptr);
};

class GraphViewWidget : public QGraphicsView
{
  Q_OBJECT
public:
  GraphViewWidget(QWidget* parent = nullptr);
  ~GraphViewWidget();
  QList<QGraphicsItem*> getSelectedItems();
  void setGraph(std::map<std::pair<int, git_oid>,
                         std::set<std::pair<int, git_oid>>> parents,
                git_oid headId,
                std::map<std::string, git_oid> branchRefs);

private slots:
  void graphChanged(Graph* graph);

public slots:
  void updatedGraph(std::map<std::pair<int, git_oid>,
                             std::set<std::pair<int, git_oid>>> parents,
                    git_oid headId,
                    std::map<std::string, git_oid> branchRefs);

private:
  CustomScene* m_scene = nullptr;
  Graph* m_graph = nullptr;
};

#endif // GRAPHVIEWWIDGET_H
