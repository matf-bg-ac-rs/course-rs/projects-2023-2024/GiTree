#include "graph.hpp"

#include "utility.hpp"
#include <QPainter>

Graph::Context::Context()
  : m_gvContext(gvContext())
{
}

Graph::Context::~Context()
{
  gvFreeContext(m_gvContext);
}

Graph::Context Graph::m_context;

Graph::Graph()
  : QGraphicsItem()
  , m_gvGraph(agopen(const_cast<char*>("Tree"), Agdirected, nullptr))
{
  set_gv_attribute(m_gvGraph, "splines", "true");
  set_gv_attribute(m_gvGraph, "rankdir", "TP");
}

Graph::~Graph()
{
  gvFreeLayout(m_context.m_gvContext, m_gvGraph);
  agclose(m_gvGraph);
}

QRectF
Graph::boundingRect() const
{
  return m_boundingRectangle;
}
void
Graph::paint(QPainter* painter,
             const QStyleOptionGraphicsItem* option,
             QWidget* widget)
{
}

bool
Graph::addNode(Node* node)
{
  if (node->parentItem())
    return false;

  node->setParentItem(this);
  node->m_gvNode = agnode(
    m_gvGraph, const_cast<char*>(std::to_string(m_nodes.size()).c_str()), 1);
  node->setup();
  m_nodes.append(node);

  return true;
}
bool
Graph::addEdge(Edge* edge, Node* src, Node* dst)
{
  if (src->parentItem() != this || dst->parentItem() != this)
    return false;

  edge->setParentItem(this);
  edge->m_gvEdge =
    agedge(m_gvGraph, src->m_gvNode, dst->m_gvNode, const_cast<char*>(""), 1);
  edge->setup();
  m_edges.append(edge);
  return true;
}
void
Graph::composeLayout()
{
  gvFreeLayout(m_context.m_gvContext, m_gvGraph);
  gvLayout(m_context.m_gvContext, m_gvGraph, "dot");

  m_boundingRectangle = QRectF();

  for (Node* n : m_nodes) {
    n->update_positions();
    m_boundingRectangle |= n->boundingRect();
  }

  for (Edge* e : m_edges) {
    e->update_positions();
    m_boundingRectangle |= e->boundingRect();
  }

  m_boundingRectangle.moveBottom(m_boundingRectangle.bottom() +
                                 m_boundingRectangle.height());
  for (auto* item : childItems())
    item->setPos({ item->x(), item->y() + m_boundingRectangle.height() });
}
