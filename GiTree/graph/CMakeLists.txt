find_package(QT NAMES Qt6 Qt5 REQUIRED COMPONENTS Widgets)
find_package(Qt${QT_VERSION_MAJOR} REQUIRED COMPONENTS Widgets)

add_library(
    graph
    node.hpp
    node.cpp
    edge.hpp
    edge.cpp
    graph.hpp
    graph.cpp
    utility.hpp
    utility.cpp
    graphview.h
    graphview.cpp
    )

target_link_libraries(
	graph
	PRIVATE Qt${QT_VERSION_MAJOR}::Widgets
	PRIVATE cgraph gvc
)

target_include_directories(
	graph
	PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}"
)
