#include "utility.hpp"

QPoint
gv_to_qt_coords(const pointf& gv_point)
{
  static const qreal dpi_adjustment = 92.0 / 72.0;
  return { static_cast<int>(dpi_adjustment * gv_point.x),
           static_cast<int>(-dpi_adjustment * gv_point.y) };
}
qreal
gv_to_qt_size(double gv_size)
{
  return 98.0 * gv_size;
}
void
set_gv_attribute(void* gv_component,
                 const QString& attribute,
                 const QString& value)
{
  agsafeset(gv_component,
            const_cast<char*>(attribute.toUtf8().constData()),
            const_cast<char*>(value.toUtf8().constData()),
            const_cast<char*>(""));
}
