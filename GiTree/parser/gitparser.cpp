#include "gitparser.h"
#include <algorithm>
#include <cstring>

gitParser* gitParser::m_parser = nullptr;

void
gitParser::checkError(int errorCode, const std::string msg)
{
  const git_error* error = git_error_last();

  std::cerr << "Error code " << errorCode << " -> " << msg << ": "
            << error->message << std::endl;

  delete error;
}

gitParser*
gitParser::getInstance(const std::string repoPath)
{
  if (m_parser == nullptr) {
    m_parser = new gitParser(repoPath);
  }

  return m_parser;
}

gitParser::gitParser(const std::string repoPath)
  : m_repoPath(repoPath)
{
  git_libgit2_init();

  int returnValue = git_repository_open(&m_repo, m_repoPath.c_str());
  if (returnValue) {
    checkError(returnValue, "Error opening the repository");
  }

  parseCommits();
  m_parents = mapParentsToChildren();
}

gitParser::~gitParser()
{
  git_repository_free(m_repo);
  git_libgit2_shutdown();
}

git_repository*
gitParser::getRepo() const
{
  return m_repo;
}

void
gitParser::parseCommits()
{

  git_branch_iterator* branchIter;
  git_reference* branchRef;
  git_branch_t branchType = GIT_BRANCH_LOCAL;

  int returnValue = git_branch_iterator_new(&branchIter, m_repo, branchType);

  if (returnValue) {
    checkError(returnValue, "Failed to create branch iterator");
  }

  while (git_branch_next(&branchRef, &branchType, branchIter) == 0) {
    const char* branchRefName = git_reference_name(branchRef);

    updateBranchRefs(branchRefName, branchRef);
    getCommitInfo(m_repo, branchRefName);

    git_reference_free(branchRef);
  }

  git_branch_iterator_free(branchIter);
}

void
gitParser::updateBranchRefs(const char* branchRefName,
                            const git_reference* branchRef)
{
  git_oid branchId;

  int returnValue = git_reference_name_to_id(&branchId, m_repo, branchRefName);
  if (returnValue) {
    checkError(returnValue, "Failed to lookup branch reference");
  }

  const char* branchName;
  returnValue = git_branch_name(&branchName, branchRef);
  if (returnValue) {
    checkError(returnValue, "Failed to get branch name");
  }
  m_branchRefs[branchName] = branchId;
}

void
gitParser::getCommitInfo(const git_repository* repo, const char* branchNameRef)
{
  git_revwalk* walker;
  git_oid oid;
  int returnValue = git_revwalk_new(&walker, m_repo);

  if (returnValue) {
    checkError(returnValue, "Failed to create revwalk");
  }

  git_reference* branchRef;
  returnValue = git_reference_lookup(&branchRef, m_repo, branchNameRef);

  if (returnValue) {
    checkError(returnValue, "Failed to lookup branch reference");
  }

  git_reference_name_to_id(&oid, m_repo, git_reference_name(branchRef));
  git_revwalk_push(walker, &oid);

  git_oid commitId;
  git_oid headId;
  returnValue = git_reference_name_to_id(&headId, m_repo, "HEAD");

  if (returnValue) {
    checkError(returnValue, "Failed to lookup HEAD reference");
  }

  git_reference* headRef;
  returnValue = git_repository_head(&headRef, m_repo);

  if (returnValue) {
    checkError(returnValue, "Failed to lookup head reference");
  }

  while (git_revwalk_next(&commitId, walker) == 0) {

    git_commit* gitCommit;
    returnValue = git_commit_lookup(&gitCommit, m_repo, &commitId);
    if (returnValue) {
      checkError(returnValue, "Failed to lookup commit");
    }

    const char* message = git_commit_summary(gitCommit);
    const git_signature* author = git_commit_author(gitCommit);
    const int time = git_commit_time(gitCommit);

    unsigned parentCount = git_commit_parentcount(gitCommit);
    std::vector<git_oid> tmp;

    bool isHeadRef = (git_oid_equal(&commitId, &headId) != 0);
    isHeadRef &= !strcmp(branchNameRef, git_reference_name(headRef));

    // Check if it's a merge commit

    // Store parent commit OIDs
    for (unsigned i = 0; i < parentCount; i++) {
      tmp.push_back(*git_commit_parent_id(gitCommit, i));
    }

    const char* branchName;
    git_branch_name(&branchName, branchRef);

    if (isHeadRef) {
      m_headId = commitId;
    }

    CommitInfo info =
      CommitInfo(commitId, message, branchName, author->name, tmp, time);

    m_commits.push_back(info);
    git_commit_free(gitCommit);
  }

  git_reference_free(headRef);
  git_reference_free(branchRef);

  git_revwalk_free(walker);
}

CommitInfo
gitParser::getCommitInfo(const char* branchRef)
{
  git_reference* branch;
  git_oid commitId;
  int returnValue = git_reference_name_to_id(&commitId, m_repo, branchRef);
  if (returnValue) {
    checkError(returnValue, "Failed to get the id.");
  }

  returnValue = git_reference_lookup(&branch, m_repo, branchRef);
  if (returnValue) {
    checkError(returnValue, "Failed to lookup branch reference.");
  }

  git_commit* gitCommit;
  returnValue = git_commit_lookup(&gitCommit, m_repo, &commitId);
  if (returnValue) {
    checkError(returnValue, "Failed to lookup commit");
  }

  const char* message = git_commit_summary(gitCommit);
  const git_signature* author = git_commit_author(gitCommit);
  const int time = git_commit_time(gitCommit);

  unsigned parentCount = git_commit_parentcount(gitCommit);
  std::vector<git_oid> tmp;

  for (unsigned i = 0; i < parentCount; i++) {
    tmp.push_back(*git_commit_parent_id(gitCommit, i));
  }

  const char* branchName;

  returnValue = git_branch_name(&branchName, branch);
  if (returnValue) {
    checkError(returnValue, "Failed to get the name of the branch.");
  }

  CommitInfo info =
    CommitInfo(commitId, message, branchName, author->name, tmp, time);

  git_commit_free(gitCommit);
  git_reference_free(branch);

  return info;
}
std::map<std::string, git_oid>
gitParser::getBranchRefs() const
{
  return m_branchRefs;
}

std::vector<CommitInfo>
gitParser::getCommits() const
{
  return m_commits;
}

std::map<std::pair<int, git_oid>, std::set<std::pair<int, git_oid>>>
gitParser::mapParentsToChildren()
{
  for (CommitInfo& commit : m_commits) {
    if (commit.getParentIds().empty())
      continue;
    for (auto parent : commit.getParentIds()) {
      m_parents[std::make_pair(commit.getTime(), parent)].insert(
        std::make_pair(commit.getTime(), commit.getId()));
    }
  }
  return m_parents;
}

git_oid
gitParser::getHeadId() const
{
  return m_headId;
}

void
gitParser::commitExecuted()
{
  git_reference* headRef;
  int returnValue = git_repository_head(&headRef, m_repo);
  if (returnValue) {
    checkError(returnValue, "Failed to get reference for HEAD");
  }

  const char* refName = git_reference_name(headRef);

  updateBranchRefs(refName, headRef);

  returnValue = git_reference_name_to_id(&m_headId, m_repo, refName);
  if (returnValue) {
    checkError(returnValue, "Failed to get the id");
  }

  m_commits.push_back(getCommitInfo(refName));
  m_parents = mapParentsToChildren();

  git_reference_free(headRef);

  emit updatedData(m_parents, m_headId, m_branchRefs);
}

void
gitParser::checkoutExecuted(const QString& branchName)
{
  const char* branchRefName =
    getReferenceName(branchName.toStdString().c_str());

  int returnValue = git_reference_name_to_id(&m_headId, m_repo, branchRefName);
  if (returnValue) {
    checkError(returnValue, "Failed to get the id");
  }

  emit updatedData(m_parents, m_headId, m_branchRefs);
}

void
gitParser::mergeExecuted(const QString& branchName)
{
  const char* branchRefName =
    getReferenceName(branchName.toStdString().c_str());

  git_reference* headRef;
  int returnValue = git_repository_head(&headRef, m_repo);
  if (returnValue) {
    checkError(returnValue, "Failed to get reference for HEAD");
  }

  const char* refName = git_reference_name(headRef);

  updateBranchRefs(refName, headRef);

  returnValue = git_reference_name_to_id(&m_headId, m_repo, refName);
  if (returnValue) {
    checkError(returnValue, "Failed to get the id");
  }

  m_commits.push_back(getCommitInfo(branchRefName));
  m_commits.push_back(getCommitInfo(refName));
  m_parents = mapParentsToChildren();

  git_reference_free(headRef);

  emit updatedData(m_parents, m_headId, m_branchRefs);
}

void
gitParser::branchWithCheckoutExecuted(const QString& branchName)
{
  git_reference* branchRef;
  const char* branchRefName =
    getReferenceName(branchName.toStdString().c_str());

  int returnValue = git_reference_name_to_id(&m_headId, m_repo, branchRefName);
  if (returnValue) {
    checkError(returnValue, "Failed to get the id");
  }

  returnValue = git_reference_lookup(&branchRef, m_repo, branchRefName);
  if (returnValue) {
    checkError(returnValue, "Failed to lookup a reference");
  }

  updateBranchRefs(branchRefName, branchRef);

  getCommitInfo(m_repo, branchRefName);
  m_parents = mapParentsToChildren();

  git_reference_free(branchRef);

  emit updatedData(m_parents, m_headId, m_branchRefs);
}

void
gitParser::branchExecuted(const QString& branchName)
{
  git_reference* branchRef;
  const char* branchRefName =
    getReferenceName(branchName.toStdString().c_str());

  int returnValue = git_reference_lookup(&branchRef, m_repo, branchRefName);
  if (returnValue) {
    checkError(returnValue, "Failed to lookup a reference");
  }

  updateBranchRefs(branchRefName, branchRef);

  getCommitInfo(m_repo, branchRefName);
  m_parents = mapParentsToChildren();

  git_reference_free(branchRef);

  emit updatedData(m_parents, m_headId, m_branchRefs);
}

const char*
gitParser::getReferenceName(const std::string branchName)
{
  git_reference* branchRef;
  int returnValue =
    git_branch_lookup(&branchRef, m_repo, branchName.c_str(), GIT_BRANCH_LOCAL);
  if (returnValue) {
    checkError(returnValue, "Failed to lookup a branch.");
  }

  const char* branchRefName = git_reference_name(branchRef);

  git_reference_free(branchRef);

  return branchRefName;
}

bool
operator<(const git_oid& oid1, const git_oid& oid2)
{
  for (size_t i = 0; i < GIT_OID_RAWSZ; ++i) {
    if (oid1.id[i] < oid2.id[i]) {
      return true;
    } else if (oid1.id[i] > oid2.id[i]) {
      return false;
    }
  }

  return false;
}

std::map<std::pair<int, git_oid>, std::set<std::pair<int, git_oid>>>
gitParser::getParents()
{
  return m_parents;
}
