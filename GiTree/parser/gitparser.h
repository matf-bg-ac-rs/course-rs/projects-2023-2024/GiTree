#ifndef GITPARSER_H
#define GITPARSER_H
#include "commitInfo.h"
#include <QObject>
#include <set>

class gitParser : public QObject
{
  Q_OBJECT
public:
  // gitParser(const std::string repoPath);
  ~gitParser();

  gitParser(gitParser& parser) = delete;
  void operator=(const gitParser&) = delete;

  static gitParser* getInstance(const std::string repoPath);

  std::vector<CommitInfo> getCommits() const;
  std::map<std::pair<int, git_oid>, std::set<std::pair<int, git_oid>>>
  mapParentsToChildren();

  git_repository* getRepo() const;
  std::map<std::pair<int, git_oid>, std::set<std::pair<int, git_oid>>>
  getParents();
  git_oid getHeadId() const;

  std::map<std::string, git_oid> getBranchRefs() const;

protected:
  gitParser(const std::string repoPath);

  static gitParser* m_parser;

public slots:
  void commitExecuted();

  void checkoutExecuted(const QString& branchName);

  void mergeExecuted(const QString& branchName);

  void branchWithCheckoutExecuted(const QString& branchName);

  void branchExecuted(const QString& branchName);

signals:
  void updatedData(std::map<std::pair<int, git_oid>,
                            std::set<std::pair<int, git_oid>>> parents,
                   git_oid headId,
                   std::map<std::string, git_oid> m_branchRefs);

private:
  std::string m_repoPath;
  git_repository* m_repo = nullptr;
  std::vector<CommitInfo> m_commits;
  std::map<std::pair<int, git_oid>, std::set<std::pair<int, git_oid>>>
    m_parents;
  git_oid m_headId;
  std::map<std::string, git_oid> m_branchRefs;

  void updateBranchRefs(const char* branchRefName,
                        const git_reference* branchRef);
  void getCommitInfo(const git_repository* repo, const char* branchName);
  void checkError(int errorCode, const std::string msg);
  CommitInfo getCommitInfo(const char* branchRef);
  const char* getReferenceName(const std::string branchName);
  void parseCommits();
};

#endif // GITPARSER_H
