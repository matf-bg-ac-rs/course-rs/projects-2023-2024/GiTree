#ifndef COMMITINFO_H
#define COMMITINFO_H
#include <git2.h>
#include <iostream>
#include <string>
#include <vector>

class CommitInfo
{
public:
  CommitInfo();
  CommitInfo(const git_oid id,
             const std::string message,
             const std::string branchName,
             const std::string author,
             const std::vector<git_oid> parentIds,
             const int time);

  git_oid getId() const;
  std::string getMessage() const;
  std::string getAuthor() const;
  std::string getBranchName() const;
  std::vector<git_oid> getParentIds() const;
  int getTime() const;

  friend std::ostream& operator<<(std::ostream& out, const CommitInfo& commit);
  friend bool operator<(const CommitInfo& commit1, const CommitInfo& commit2);

private:
  git_oid m_id;
  std::string m_message;
  std::string m_branchName;
  int m_time;
  std::string m_author;
  std::vector<git_oid> m_parentIds;
};

#endif // COMMITINFO_H
