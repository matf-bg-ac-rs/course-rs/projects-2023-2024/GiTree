#include "commitInfo.h"

CommitInfo::CommitInfo(const git_oid id,
                       const std::string message,
                       const std::string branchName,
                       const std::string author,
                       const std::vector<git_oid> parentIds,
                       const int time)
  : m_id(id)
  , m_message(message)
  , m_branchName(branchName)
  , m_author(author)
  , m_parentIds(parentIds)
  , m_time(time)
{
}

CommitInfo::CommitInfo() {}

git_oid
CommitInfo::getId() const
{
  return m_id;
}

std::string
CommitInfo::getMessage() const
{
  return m_message;
}

std::vector<git_oid>
CommitInfo::getParentIds() const
{
  return m_parentIds;
}

std::string
CommitInfo::getAuthor() const
{
  return m_author;
}

std::string
CommitInfo::getBranchName() const
{
  return m_branchName;
}

int
CommitInfo::getTime() const
{
  return m_time;
}

std::ostream&
operator<<(std::ostream& out, const CommitInfo& commit)
{

  git_oid id = commit.getId();
  out << "Branch name: " << commit.getBranchName() << std::endl;
  out << "Author: " << commit.getAuthor() << std::endl;
  out << "Commit id: " << git_oid_tostr_s(&id) << std::endl;
  out << "Commit message: " << commit.getMessage() << std::endl;
  out << "Parents:" << std::endl;
  for (const auto& parentId : commit.getParentIds()) {
    out << "    " << git_oid_tostr_s(&parentId) << std::endl;
  }

  return out;
}

bool
operator<(const CommitInfo& commit1, const CommitInfo& commit2)
{
  return commit1.getTime() < commit2.getTime();
}
