#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget* parent)
  : QMainWindow(parent)
  , ui(new Ui::MainWindow)
{
  //    ui->setupUi(this);
  //    this->setFixedSize(this->size());
}

MainWindow::~MainWindow()
{
  delete m_commands;
  delete m_parser;
  delete ui;
}

qint8
MainWindow::getNumberOfConflictingFiles()
{
  return m_numberOfConflictingFiles;
}

void
MainWindow::receiveData(const QString& userName,
                        const QString& userEmail,
                        const QString& path)
{
  ui->setupUi(this);
  this->setFixedSize(this->size());
  m_userName = userName;
  m_userEmail = userEmail;
  m_path = path;
  QDir dir = QDir(this->m_path);
  ui->lbRepoName->setText(dir.dirName());
  QString new_path = dir.absolutePath();
  ui->treeWidget->setViewPath(new_path);
  m_commands = new GitCommands(
    m_path.toStdString(), m_userName.toStdString(), m_userEmail.toStdString());
  if (!hasGitFolder(m_path)) {
    m_commands->initializeRepository();
  }
  m_commands->openRepository();
  m_parser = gitParser::getInstance(this->m_path.toStdString());

  connect(this, SIGNAL(commitExecuted()), m_parser, SLOT(commitExecuted()));
  connect(this,
          SIGNAL(checkoutExecuted(QString)),
          m_parser,
          SLOT(checkoutExecuted(QString)));
  connect(this,
          SIGNAL(mergeExecuted(QString)),
          m_parser,
          SLOT(mergeExecuted(QString)));
  connect(this,
          SIGNAL(branchWithCheckoutExecuted(QString)),
          m_parser,
          SLOT(branchWithCheckoutExecuted(QString)));
  connect(this,
          SIGNAL(branchExecuted(QString)),
          m_parser,
          SLOT(branchExecuted(QString)));

  connect(
    m_parser,
    SIGNAL(updatedData(
      std::map<std::pair<int, git_oid>, std::set<std::pair<int, git_oid>>>,
      git_oid,
      std::map<std::string, git_oid>)),
    ui->graphicsView,
    SLOT(updatedGraph(
      std::map<std::pair<int, git_oid>, std::set<std::pair<int, git_oid>>>,
      git_oid,
      std::map<std::string, git_oid>)));

  connect(this, SIGNAL(emptyConflictsList()), this, SLOT(mergeBranches()));
  ui->graphicsView->setGraph(
    m_parser->getParents(), m_parser->getHeadId(), m_parser->getBranchRefs());
  ui->graphicsView->show();
  this->show();
}

void
MainWindow::closeTab(QString filePath)
{
  QDir baseDirPath(m_path);
  QString relativeFilePath = baseDirPath.relativeFilePath(filePath);
  m_commands->addPathToIndex(relativeFilePath.toStdString(), true);
  ui->graphTabSelect->removeTab(ui->graphTabSelect->currentIndex());
  m_numberOfConflictingFiles--;
  if (m_numberOfConflictingFiles == 0)
    emit emptyConflictsList();
}

void
MainWindow::mergeBranches()
{
  m_commands->executeMergeCommit("Resolved conflicts and merged",
                                 ui->lineEdit->text().toStdString());
  emit mergeExecuted(ui->lineEdit->text());
}

void
MainWindow::on_pbAdd_clicked()
{
  // Calls GitCommands::addFileToIndex and forwards it path to selected file or
  // directory from ShowRepoWidget.
  bool isFile = true;
  if (ui->treeWidget->getSelectedItemPath().isEmpty()) {
    QMessageBox::warning(this, "Error", "Nothing is selected");
    return;
  }
  QFileInfo info(ui->treeWidget->getSelectedItemPath(false));
  if (!info.isFile())
    isFile = false;
  m_commands->addPathToIndex(
    ui->treeWidget->getSelectedItemPath().toStdString(), isFile);
}

void
MainWindow::on_pbCheckout_clicked()
{
  const QString text = ui->lineEdit->text();
  // Calls GitCommands::checkoutToBranch and forwards it a existing branch name
  // from lineEdit.
  if (text.isEmpty()) {
    QMessageBox::warning(this, "Error", "Branch name required");
    return;
  }
  if (m_commands->checkoutToBranch(text.toStdString()) ==
      errorCodes::ENOTFOUND) {
    QMessageBox::warning(this, "Error", "Branch doesn't exist");
    ui->lineEdit->clear();
    return;
  }
  emit checkoutExecuted(text);
  ui->lineEdit->clear();
}

void
MainWindow::on_pbBranchWithCheckout_clicked()
{
  // Calls GitCommands::createBranchAndCheckout and forwards it the branch name,
  // from lineEdit, which will then be created and moved to.
  const QString text = ui->lineEdit->text();
  if (text.isEmpty()) {
    QMessageBox::warning(this, "Error", "Branch name required");
    return;
  }
  errorCodes::Indicator ind =
    m_commands->createBranchAndCheckout(text.toStdString());
  if (ind == errorCodes::EINVALIDSPEC) {
    QMessageBox::warning(this, "Error", "Branch name not valid");
    ui->lineEdit->clear();
    return;
  }
  if (ind == errorCodes::EEXISTS) {
    QMessageBox::warning(this, "Error", "Branch already exists");
    ui->lineEdit->clear();
    return;
  }

  emit branchWithCheckoutExecuted(text);
  ui->lineEdit->clear();
}

void
MainWindow::on_pbBranch_clicked()
{
  // Calls GitCommands::createBranch and forwards it the branch name, from
  // lineEdit, to be created.
  const QString text = ui->lineEdit->text();
  if (text.isEmpty()) {
    QMessageBox::warning(this, "Error", "Branch name required");
    return;
  }
  errorCodes::Indicator ind = m_commands->createBranch(text.toStdString());
  if (ind == errorCodes::EINVALIDSPEC) {
    QMessageBox::warning(this, "Error", "Branch name not valid");
    ui->lineEdit->clear();
    return;
  }
  if (ind == errorCodes::EEXISTS) {
    QMessageBox::warning(this, "Error", "Branch already exists");
    ui->lineEdit->clear();
    return;
  }

  emit branchExecuted(text);
  ui->lineEdit->clear();
}

void
MainWindow::on_pbMerge_clicked()
{
  // Calls GitCommands::gitMerge and forwards it two selected branches, from gui
  // graphics scene, to merge or start a merge conflict handler window.
  const QString text = ui->lineEdit->text();
  std::vector<std::string> conflictsList;
  if (text.isEmpty()) {
    QMessageBox::warning(this, "Error", "Branch name required");
    return;
  }
  errorCodes::Indicator ind =
    m_commands->executeMerge(text.toStdString(), conflictsList);
  if (ind == errorCodes::ENOTFOUND) {
    QMessageBox::warning(this, "Error", "Branch doesn't exist");
    ui->lineEdit->clear();
    return;
  }
  if (ind == errorCodes::EUPTODATE) {
    QMessageBox::warning(this, "Error", "Already up-to-date");
    ui->lineEdit->clear();
    return;
  }
  if (ind == errorCodes::ECONFLICTS) {
    if (conflictsList.size() > 10) {
      QMessageBox::warning(
        this, "Error", "Too many conflicting files, resolve locally");
      return;
    } else {
      QMessageBox::warning(this, "Error", "Resolve conflicts");
      QDir dir(m_path);
      QString file, file_path;
      m_numberOfConflictingFiles = conflictsList.size();
      for (std::string& conflict : conflictsList) {
        file = QString::fromStdString(conflict);
        file_path = dir.absoluteFilePath(file);
        MergeConflictWidget* page = new MergeConflictWidget(ui->graphTabSelect);
        page->setPath(file_path);
        ui->graphTabSelect->addTab(page, file);
      }
      return;
    }
  }

  emit mergeExecuted(text);
  ui->lineEdit->clear();
}

void
MainWindow::on_pbCommit_clicked()
{
  // Calls GitCommands::executeCommit and forwards it the commit message from
  // teCommitMesssage.
  if (ui->teCommitMessage->toPlainText().isEmpty()) {
    QMessageBox::warning(this, "Error", "Commit message required");
    return;
  }

  if (m_commands->executeCommit(
        ui->teCommitMessage->toPlainText().toStdString()) ==
      errorCodes::EINDEXEMPTY) {
    QMessageBox::warning(this, "Error", "Nothing to commit");
    return;
  }

  emit commitExecuted();
  ui->teCommitMessage->clear();
}

bool
MainWindow::hasGitFolder(const QString& directoryPath)
{
  QDir directory(directoryPath);
  QString gitFolderPath = directory.filePath(".git");
  bool hasGitFolder = QDir(gitFolderPath).exists();

  return hasGitFolder;
}
