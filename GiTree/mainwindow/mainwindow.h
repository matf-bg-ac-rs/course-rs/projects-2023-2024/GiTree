#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "gitcommands.h"
#include "parser/gitparser.h"
#include "commands/mergeconflict.h"
#include <QMainWindow>
#include <QMessageBox>
#include <QTimer>
#include <vector>

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  MainWindow(QWidget* parent = nullptr);
  ~MainWindow();
  qint8 getNumberOfConflictingFiles();
public slots:
  void receiveData(const QString& userName,
                   const QString& userEmail,
                   const QString& path);
  void closeTab(QString filePath);
  void mergeBranches();

signals:
  void commitExecuted();

  void checkoutExecuted(const QString branchName);

  void mergeExecuted(const QString branchName);

  void branchWithCheckoutExecuted(const QString branchName);

  void branchExecuted(const QString branchName);

  void conflictsOccurred();

  void emptyConflictsList();
private slots:
  void on_pbAdd_clicked();

  void on_pbCheckout_clicked();

  void on_pbBranchWithCheckout_clicked();

  void on_pbBranch_clicked();

  void on_pbMerge_clicked();

  void on_pbCommit_clicked();

private:
  Ui::MainWindow* ui;
  GitCommands* m_commands = nullptr;
  QString m_userName;
  QString m_userEmail;
  QString m_path;
  qint8 m_numberOfConflictingFiles;
  gitParser* m_parser = nullptr;
  bool hasGitFolder(const QString& directoryPath);
};
#endif // MAINWINDOW_H
