#include "catch.hpp"
#include "gitparser.h"

TEST_CASE("Testing constructor for gitParser class", "[gitParser]")
{
  SECTION("Path to non existent repository opening is not succesful")
  {
    const char* repoPath = "nepostojeca_putanja";

    gitParser* result = gitParser::getInstance(repoPath);

    REQUIRE(result->getRepo() == nullptr);
  }

  SECTION("Succesful initializing if the path is a repository")
  {
    const std::string repoPath = "/TestDir/Branch";

    gitParser* result = gitParser::getInstance(repoPath);

    REQUIRE(result->getRepo() != nullptr);
  }

  SECTION("Cannot use a copy constructor")
  {
    const std::string repoPath = "/TestDir/Branch";

    gitParser* expected = gitParser::getInstance(repoPath);

    gitParser* result(expected);

    REQUIRE(result == nullptr);
  }

  SECTION("Cannot use a assignment operator")
  {
    const std::string repoPath = "/TestDir/Branch";

    gitParser* expected = gitParser::getInstance(repoPath);

    gitParser* result = expected;

    REQUIRE(result == nullptr);
  }
}
