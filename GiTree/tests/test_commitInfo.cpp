#include "catch.hpp"
#include "parser/commitInfo.h"

TEST_CASE("Testing constructor for CommitInfo class")
{
  SECTION("Input check")
  {
    git_oid id;
    std::string message = "Test message";
    std::string branchName = "main";
    std::string author = "J. K. Rowling";
    int time = 123456789;

    std::vector<git_oid> parentIds;
    git_oid parent1;
    git_oid_fromstr(&parent1, "27456e8aeb1d5f7be2921c09681f4be0ed10e302");
    parentIds.push_back(parent1);

    CommitInfo commitInfo(id, message, branchName, author, parentIds, time);
    git_oid commitId = commitInfo.getId();

    REQUIRE(git_oid_equal(&id, &commitId));
    REQUIRE(commitInfo.getMessage() == message);
    REQUIRE(commitInfo.getBranchName() == branchName);
    REQUIRE(commitInfo.getAuthor() == author);

    for (int i = 0; i < parentIds.size(); i++) {
      git_oid parentId = commitInfo.getParentIds()[i];
      REQUIRE(git_oid_equal(&parentId, &parentIds[i]));
    }

    REQUIRE(commitInfo.getTime() == time);
  }
}
