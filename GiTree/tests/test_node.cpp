#include "../graph/node.hpp"
#include "catch.hpp"

TEST_CASE("Testing constructor for Node class")
{
  qreal width = 50.0;
  qreal height = 30.0;
  QString label = "Test Node";
  QFont labelFont("Arial", 12);

  SECTION("Input check")
  {
    Node node(width, height, label, labelFont);

    REQUIRE(node.getWidth() == width);
    REQUIRE(node.getHeight() == height);
    REQUIRE(node.getLabel() == label);
    REQUIRE(node.getLabelFont() == labelFont);
  }
}
