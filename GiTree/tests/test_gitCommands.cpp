#include "catch.hpp"
#include "gitcommands.h"

TEST_CASE("gitCommands::executeCommit() - Commit staged items to index")
{
  SECTION("No staged items")
  {
    GitCommands commands("/TestDirs/NoStaged", "userName", "user@gmail.com");
    errorCodes::Indicator expected = errorCodes::EINDEXEMPTY;

    errorCodes::Indicator result = commands.executeCommit("No staged");

    REQUIRE(result == expected);
  }
  SECTION("One or more staged items")
  {
    GitCommands commands(
      "/TestDirs/OneOrMoreStaged", "userName", "user@gmail.com");
    errorCodes::Indicator expected = errorCodes::SUCCESS;

    errorCodes::Indicator result = commands.executeCommit("One or more staged");

    REQUIRE(result == expected);
  }
}
TEST_CASE("gitCommands::createBranch() - Create a new branch")
{
  SECTION("Branch already exists")
  {
    GitCommands commands("/TestDirs/Branch", "userName", "user@gmail.com");
    std::string branchName = "existingBranch";
    errorCodes::Indicator expected = errorCodes::EEXISTS;

    errorCodes::Indicator result = commands.createBranch(branchName);

    REQUIRE(result == expected);
  }
  SECTION("Invalid name format")
  {
    GitCommands commands("/TestDirs/Branch", "userName", "user@gmail.com");
    std::string branchName = "branch..";
    errorCodes::Indicator expected = errorCodes::EINVALIDSPEC;

    errorCodes::Indicator result = commands.createBranch(branchName);

    REQUIRE(result == expected);
  }
  SECTION("Branch name doesn't exist")
  {
    GitCommands commands("/TestDirs/Branch", "userName", "user@gmail.com");
    std::string branchName = "newBranch";
    errorCodes::Indicator expected = errorCodes::SUCCESS;

    errorCodes::Indicator result = commands.createBranch(branchName);

    REQUIRE(result == expected);
  }
}
TEST_CASE("gitCommands::checkoutToBranch - Checkout to existing branch")
{
  SECTION("Branch name does not exist")
  {
    GitCommands commands("/TestDirs/Branch", "userName", "user@gmail.com");
    std::string branchName = "newBranch";
    errorCodes::Indicator expected = errorCodes::ENOTFOUND;

    errorCodes::Indicator result = commands.checkoutToBranch(branchName);

    REQUIRE(result == expected);
  }
  SECTION("Branch exists")
  {
    GitCommands commands("/TestDirs/Branch", "userName", "user@gmail.com");
    std::string branchName = "existingBranch";
    errorCodes::Indicator expected = errorCodes::SUCCESS;

    errorCodes::Indicator result = commands.checkoutToBranch(branchName);

    REQUIRE(result == expected);
  }
}
TEST_CASE("gitCommands::executeMerge() - Merge two branches")
{
  SECTION("Branch name does not exist")
  {
    GitCommands commands("/TestDirs/Branch", "userName", "user@gmail.com");
    std::string branchName = "newBranch";
    errorCodes::Indicator expected = errorCodes::ENOTFOUND;
    std::vector<std::string> conflictsList;

    errorCodes::Indicator result =
      commands.executeMerge(branchName, conflictsList);

    REQUIRE(conflictsList.size() == 0);
    REQUIRE(result == expected);
  }
  SECTION("No conflicts")
  {
    GitCommands commands("/TestDirs/NoConflicts", "userName", "user@gmail.com");
    std::string branchName = "newBranch";
    errorCodes::Indicator expected = errorCodes::SUCCESS;
    std::vector<std::string> conflictsList;

    errorCodes::Indicator result =
      commands.executeMerge(branchName, conflictsList);

    REQUIRE(conflictsList.size() == 0);
    REQUIRE(result == expected);
  }
  SECTION("Nothing to merge/Already up to date")
  {
    GitCommands commands(
      "/TestDirs/NothingToMerge", "userName", "user@gmail.com");
    std::string branchName = "existingBranch";
    errorCodes::Indicator expected = errorCodes::EUPTODATE;
    std::vector<std::string> conflictsList;

    errorCodes::Indicator result =
      commands.executeMerge(branchName, conflictsList);

    REQUIRE(conflictsList.size() == 0);
    REQUIRE(result == expected);
  }
  SECTION("One or more conflicts")
  {
    GitCommands commands(
      "/TestDirs/MergeConflicts", "userName", "user@gmail.com");
    std::string branchName = "existingBranch";
    errorCodes::Indicator expected = errorCodes::ECONFLICTS;
    std::vector<std::string> conflictsList;

    errorCodes::Indicator result =
      commands.executeMerge(branchName, conflictsList);

    REQUIRE(conflictsList.size() != 0);
    REQUIRE(result == expected);
  }
}
TEST_CASE(
  "gitCommands::getConflictingFiles() - Get conflicting files list (vector)")
{
  SECTION("No conflicting files")
  {
    GitCommands commands("/TestDirs/NoConflicts", "userName", "user@gmail.com");

    std::vector<std::string> result = commands.getConflictingFiles();

    REQUIRE(result.size() == 0);
  }
  SECTION("One or more conflicting files")
  {
    GitCommands commands(
      "/TestDirs/MergeConflicts", "userName", "user@gmail.com");
    std::vector<std::string> expected{ "fajl1" };

    std::vector<std::string> result = commands.getConflictingFiles();

    REQUIRE(result.size() == expected.size());
    for (int i = 0; i < result.size(); i++) {
      REQUIRE(expected[i] == result[i]);
    }
  }
}
TEST_CASE("gitCommands::gitCommands() - gitCommands constructor")
{
  SECTION("Normal constructor")
  {
    std::string expPath;
    std::string expUserName;
    std::string expUserEmail;

    GitCommands result = GitCommands(expPath, expUserName, expUserEmail);

    REQUIRE(result.getPath() == expPath);
    REQUIRE(result.getUserName() == expUserName);
    REQUIRE(result.getUserEmail() == expUserEmail);
  }
}
