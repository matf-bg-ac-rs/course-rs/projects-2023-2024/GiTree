#include "../graph/edge.hpp"
#include "catch.hpp"

TEST_CASE("Testing the constructor for Edge class")
{
  SECTION("Edge is constructed with valid parameters")
  {
    QString label = "TestLabel";
    QFont labelFont("Arial", 12);

    Edge edge(label, labelFont);

    REQUIRE(edge.getLabel() == label);
    REQUIRE(edge.getLabelFont() == labelFont);
  }
}
