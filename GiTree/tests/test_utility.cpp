#include "catch.hpp"
#include "utility.hpp"

TEST_CASE("Utility Functions Test")
{
  SECTION("gv_to_qt_coords - Convert gv_point to QPoint")
  {
    pointf gv_point = { 10.0, 20.0 };
    QPoint result = gv_to_qt_coords(gv_point);

    REQUIRE(result == QPoint(115, -184));
  }

  SECTION("gv_to_qt_size - Convert gv_size to qreal")
  {
    double gv_size = 2.0;
    qreal result = gv_to_qt_size(gv_size);

    REQUIRE(result == Approx(196.0));
  }
}
