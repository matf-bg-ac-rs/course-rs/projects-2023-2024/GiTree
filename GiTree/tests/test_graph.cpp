#include "../graph/edge.hpp"
#include "../graph/graph.hpp"
#include "../graph/node.hpp"
#include "catch.hpp"

TEST_CASE("Graph addEdge Test")
{
  SECTION("Nodes are valid, edge is added")
  {
    Graph graph;
    Node* srcNode;
    Node* dstNode;
    Edge* edge;

    bool result = graph.addEdge(edge, srcNode, dstNode);

    REQUIRE(result == true);
    REQUIRE(edge->parentItem() == &graph);
  }

  SECTION("Source node is not a child of the graph, edge is not added")
  {
    Graph graph;
    Node* srcNode;
    Node* dstNode;
    Edge* edge;

    Graph otherGraph;
    srcNode->setParentItem(&otherGraph);

    bool result = graph.addEdge(edge, srcNode, dstNode);

    REQUIRE(result == false);
    REQUIRE(edge->parentItem() == nullptr);
  }
}
