#include "startwindow.h"
#include "./ui_startwindow.h"

#include <QDebug>
#include <QDir>
#include <QLabel>
#include <QLineEdit>
#include <QMessageBox>
#include <QPushButton>
#include <QRegularExpression>
#include <QString>
#include <QTemporaryFile>
#include <QValidator>

StartWindow::StartWindow(QWidget* parent)
  : QMainWindow(parent)
  , ui(new Ui::StartWindow)
{
  ui->setupUi(this);
  ui->leFilePath->setReadOnly(true);
  this->setFixedSize(this->size());

  this->setWindowTitle("Start Window");

  m_browser = new Browser(this);

  QObject::connect(
    m_browser, SIGNAL(sendPath(QString&)), this, SLOT(receivePath(QString&)));
}

StartWindow::~StartWindow()
{
  delete m_browser;
  delete ui;
}

void
StartWindow::receivePath(QString& currentPath)
{
  ui->leFilePath->setText(currentPath);
}

void
StartWindow::on_pbNext_clicked()
{

  QString userName = ui->leName->text();
  QString userEmail = ui->leEmail->text();
  QString path = ui->leFilePath->text();
  QDir dir(path);

  QRegularExpression rx(
    "\\b[\\w.-]+@[\\w.-]+(\\.[\\w.-]+)*\\.[A-Za-z]{2,4}\\b");
  QRegularExpressionValidator* validator =
    new QRegularExpressionValidator(rx, this);
  ui->leEmail->setValidator(validator);
  int pos = 0;
  QValidator::State result = validator->validate(userEmail, pos);

  if (userEmail.length() == 0 && userName.length() == 0 && path.length() == 0) {
    QMessageBox::warning(
      this, "Error", "Name, email and directory path are required.");
    ui->leName->setFocus();
    return;
  }

  if (userName.length() == 0) {
    QMessageBox::warning(this, "Error", "Your name is required.");
    ui->leName->setFocus();
    return;
  }

  if (userEmail.length() == 0) {
    QMessageBox::warning(this, "Error", "Your Email is required.");
    ui->leEmail->setFocus();
    return;
  }

  if (result != QValidator::Acceptable) {
    QMessageBox::warning(
      this,
      "Error",
      "Invalid email format. Please enter a valid email address.");
    ui->leEmail->clear();
    ui->leEmail->setFocus();
    return;
  }

  if (path.length() == 0) {
    QMessageBox::warning(this, "Error", "Directory path is required.");
    ui->pbBrowse->setFocus();
    return;
  }

  if (userEmail.length() > 64 && userName.length() > 40) {
    QMessageBox::warning(this,
                         "Error",
                         "You have exceeded the allowed limit of maximum "
                         "characters for your Email and Name.");
    ui->leName->clear();
    ui->leName->setFocus();
    ui->leEmail->clear();
    ui->leEmail->setFocus();
    return;
  }

  if (userName.length() > 40) {
    QMessageBox::warning(this,
                         "Error",
                         "You have exceeded the allowed limit of maximum "
                         "characters for your Name.");
    ui->leName->clear();
    ui->leName->setFocus();
    return;
  }

  if (userEmail.length() > 64) {
    QMessageBox::warning(this,
                         "Error",
                         "You have exceeded the allowed limit of maximum "
                         "characters for your Email.");
    ui->leEmail->clear();
    ui->leEmail->setFocus();
    return;
  }

  if (!dir.exists()) {
    QMessageBox::warning(this, "Error", "You have not selected a directory.");
    ui->leFilePath->clear();
    ui->pbBrowse->setFocus();
    return;
  }

  emit sendData(userName, userEmail, path);
  this->close();
}

void
StartWindow::on_pbBrowse_clicked()
{
  m_browser->show();
}
