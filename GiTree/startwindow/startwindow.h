#ifndef STARTWINDOW_H
#define STARTWINDOW_H

#include <QLabel>
#include <QLineEdit>
#include <QMainWindow>
#include <QMessageBox>
#include <QPushButton>
#include <QTemporaryFile>

#include "browser/browser.h"

class QLineEdit;

QT_BEGIN_NAMESPACE
namespace Ui {
class StartWindow;
}
QT_END_NAMESPACE

class StartWindow : public QMainWindow
{
  Q_OBJECT

public:
  StartWindow(QWidget* parent = nullptr);
  ~StartWindow();

signals:
  void sendData(const QString& userName,
                const QString& userEmail,
                const QString& path);

public slots:
  void receivePath(QString& currentPath);

private slots:
  void on_pbNext_clicked();

  void on_pbBrowse_clicked();

private:
  Ui::StartWindow* ui;
  Browser* m_browser;
};
#endif // STARTWINDOW_H
