# GiTree

Alat koji omogućava izvršavanje git komandi preko interakcije sa grafom. Graf predstavlja vizuelni prikaz istorije komitova nekog repozitorijuma. 

Članovi:
 - <a href="https://gitlab.com/rexi12345">Relja Pešić 73/2019</a>
 - <a href="https://gitlab.com/Milica19105">Milica Tošić 105/2019</a>
 - <a href="https://gitlab.com/tomiy7">Radenko Nikolić 400/2021</a>
 - <a href="https://gitlab.com/pavle138">Pavle Dušanić 287/2019</a>
 - <a href="https://gitlab.com/Koc13">Ilija Kočinac 55/2019</a>
 - <a href="https://gitlab.com/jelena3160">Jelena Lazović 288/2019</a>

 ## Reference:
 - Materijali sa vežbi : <br />
<a href="https://gitlab.com/matf-bg-ac-rs/course-rs/lab-2021-2022">Gitlab repozitorijum za vežbe iz Razvoja softvera.</a><br />
<a href="https://gitlab.com/matf-bg-ac-rs/course-azrs/MATF-AZRS/-/tree/main">Gitlab repozitorijum za vežbe iz Alata za razvoj softvera.</a>


- Materijali sa predavanja : <br />
<a href="https://poincare.matf.bg.ac.rs/~sasa.malkov/download.php?dpth=1&cap=Razvoj+softvera&bp=rs.r290.2023%2Fpublic&rp=%2Fpredavanja">Sajt profesora Saše Malkova.</a><br />
<a href="https://matf.cukic.co/?content=azrs">Sajt profesora Ivana Čukića.</a>

- Alat za crtanje dijagrama i grafičkih prikaza :<br />
<a href="https://app.diagrams.net/">draw.io</a>

- Dokumentacija za C++:<br />
<a href="https://en.cppreference.com/w/">cppreference</a>

- Softver za vizuelizaciju grafa :<br />
<a href="https://graphviz.org/">Graphviz</a>

- QT dokumentacija :<br />
<a href="https://doc.qt.io/qt-6/classes.html">Sve QT C++ klase.</a>

- libgit2 dokumentacija :<br />
<a href="https://libgit2.org/libgit2/#HEAD">libgit2</a>

## Demo snimak

- <a href="https://www.youtube.com/watch?v=dlTF9m2gDFs">Link ka snimku</a>

## Potrebne instalacije
- <a href="https://graphviz.org/download/">Instalacija Graphviz-a</a>

- <a href="https://libgit2.org/">Link ka libgit2(potrebno kloniranje za instalaciju)</a>

